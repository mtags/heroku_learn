//https://css-tricks.com/gulp-for-beginners/
var gulp = require('gulp');
// var sass = require('gulp-sass');
// var browserSync = require('browser-sync').create();
// var useref = require('gulp-useref');
// var uglify = require('gulp-uglify');
// var gulpIf = require('gulp-if');
// var cleanCss = require('gulp-clean-css');
// var imagemin = require('gulp-imagemin');
// var cache = require('gulp-cache');
// var del = require('del');
var runSequence = require('run-sequence');
// var ts = require('gulp-typescript');
// var modRewrite  = require('connect-modrewrite');
var install = require("gulp-install");


gulp.task('hello', function(){
    console.log('gulp is working');
});

// gulp.task('sass', function(){
//     gulp.src('public-web/scss/**/*.scss')
//     .pipe(sass()) // Using gulp-sass
//     .pipe(gulp.dest('public-web/css'))
//     .pipe(browserSync.reload({stream:  true}))
// });


// gulp.task('watch', ['browserSync', 'sass', 'ts:compile'], function(){
//     gulp.watch('public-web/scss/**/*.scss', ['sass']);
//     gulp.watch('public-web/**/*.ts', ['ts:compile']);
    
//     gulp.watch('public-web/**/.html').on('change', browserSync.reload);
//     gulp.watch('public-web/**/*.js', browserSync.reload);
// });

// gulp.task('browserSync', function(){
//     browserSync.init({
//         server:{baseDir:  'public-web',
//                 middleware: [
//                     modRewrite([
//                     '!\\.\\w+$ /index.html [L]'
//                     ])
//                 ]
//                }
//     });
// });

// gulp.task('useref', function(){
//     gulp.src('public-web/*.html')
//     .pipe(useref())    
//     .pipe(gulpIf('*.js', uglify()))
//     .pipe(gulpIf('*.css', cleanCss()))
//     .pipe(gulp.dest('dist'))
// });

// gulp.task('images', function(){
//     gulp.src('public-web/images/**/*.+(png|jpg|gif|svg)')
//     .pipe(cache(imagemin({interlaced: true})))
//     .pipe(gulp.dest('dist/images'));
// });

// gulp.task('fonts', function() {
//   return gulp.src('public-web/fonts/**/*')
//   .pipe(gulp.dest('dist/fonts'))
// });

// gulp.task('clean:dist', function() {
//   return del.sync('dist');
// });

// gulp.task('cache:clear', function (callback) {
//     return cache.clearAll(callback)
// });


// var tsProjectSystem = ts.createProject('tsconfig.json', { "module": "system" });
// var tsProjectCommonJS = ts.createProject('tsconfig.json', { "module": "commonjs" });

// gulp.task('ts:compile', function() {
// 	var tsResultSystem = gulp.src('public-web/**/*.ts')
// 					.pipe(ts(tsProjectSystem));	
// 	tsResultSystem.js.pipe(gulp.dest('public-web'));
    
//     var tsResultCommonJS = gulp.src(['server/**/*.ts', '!node_modules/**', '!typings/**', '!public-web/**'])
// 					.pipe(ts(tsProjectCommonJS));	
// 	tsResultCommonJS.js.pipe(gulp.dest(''));
// });


// gulp.task('build', function (callback) {
//   runSequence('clean:dist', 
//     ['sass', 'useref', 'images', 'fonts'],
//     callback
//   )
// });

// gulp.task('default', function (callback) {
//   runSequence(['sass', 'ts:compile', 'browserSync', 'watch'],
//     callback
//   )
// });



gulp.task('default', function (callback) {
  runSequence(['hello'],
    callback
  )
});

gulp.task('web:install', function () {
  gulp.src(__dirname + '/web/**')
  .pipe(gulp.dest('./web'))
  .pipe(install());
});

gulp.task('server:install', function () {
  gulp.src(__dirname + '/server/**')
  .pipe(gulp.dest('./server'))
  .pipe(install());
});


gulp.task('heroku:deploy', function (callback) {
  runSequence(['hello', 'web:install', 'server:install'],
    callback
  )
});





